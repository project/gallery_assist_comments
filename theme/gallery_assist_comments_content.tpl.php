<?php

/**
 * @file gallery_assist_comments_content.tpl.php
 * Default theme implementation to display the comments content from a gallery item.
 *
 * @param string $content
 *   The HTML formatted comments content.
 *
 * @see theme_gallery_assist_comments_content()
 */
?>
  <?php print $content; ?>