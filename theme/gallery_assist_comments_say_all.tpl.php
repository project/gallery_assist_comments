<?php

/**
 * @file gallery_assist_comments_say_all.tpl.php
 * Default theme implementation to display the text for the "Display all comments" link.
 *
 * @see theme_gallery_assist_comments_say_all()
 */
?>
All comments