<?php

/**
 * @file gallery_assist_comments_form_box.tpl.php
 * Default theme implementation to display the comment form.
 *
 * @param string $content
 *   The rendered comment form content.
 *
 * @see theme_gallery_assist_comments_form_box()
 */
?>
<?php print $content; ?>