<?php

/**
 * @file gallery_assist_comments_say_fewer.tpl.php
 * Default theme implementation to display the text for the "Display fewer comments" link.
 *
 * @see theme_gallery_assist_comments_say_fewer()
 */
?>
Fewer comments